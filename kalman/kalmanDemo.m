clear s
dt = 0.1;
s.A = [1 dt; 0 1]; 
s.B = [dt^2/2; dt]; 
s.Q = diag([0.1^4 0.1^2]);
s.H = [1 0]; 
s.R = 0.2^2;

s.x = [0; 0];
s.P = s.Q;

x=zeros(2,20);
for i=1:30
    s(end).u = randn*0.1+0.2;
    x(:,i+1) = s(end).A*x(:,i)+s(end).B*s(end).u;
    s(end).z = s(end).H*x(:,i+1) + randn*0.2;
    s(end+1)=kalmanf(s(end));
end


figure
hold on
grid on
xKal = [s(2:end).x];
z = [s(1:end-1).z];
% plot measurement data:
hz=plot(z,'r.');
% plot a-posteriori state estimates:
hk=plot(xKal(1,:),'b-');
ht=plot(x(1,:),'g-');
legend([hz hk ht],'observations','Kalman output','true position')
title('Position Estimation Example')
hold off

% x = A*x + B*u;
% P = A*P*A' + Q;
% 
% K = P*H/(H*P*H'+R);
% 
% x = x + K*(v - H*x);
% P = (P - K*H)*P;