%% MODEL
% x = [alfa omega_bias]
% alfa(k)= alfa(k-1) + (omega(k-1)-omega_bias(k-1))*dt
% omega_bias(k) = omega_bias(k-1) 

%% INIT
load Kalman_data             % T - czas; A_ - akcelerometr, V_ - zyroskop

%% Plots
subplot(2,1,1)
plot(T,A_,'g')
grid on, xlabel('time[s]'), ylabel('deg[\circ]'),
title('Accelerometer')
legend('raw accelerometer')
subplot(2,1,2)

plot(T,V_,'g')
legend('raw gyro')
title('Gyro')
grid on, xlabel('time[s]'), ylabel('dps[\circ/s]'), 

