tic
%% Initial values
% Sliding mode
%A=[0 1; 0 0]; B=[0; 1]; C=[-1, -1]; x100=1; x10d=.25; x200=1;x20d=.5; Tf=10; h=1;

% Limit cycle
A=[0 1; 0 -.1]; B=[0; 1]; C=[-1 0]; x100=5; x10d=5; x200=2;x20d=2; Tf=100; h=1;

%A=[-.5 1; -1 -.5]; B=[0; 1]; C=[-1, 0];
%A=[-.1 1; -1 -.1]; B=[0; 1]; C=[-1, 0];
%A=[0 1; -100 0]; B=[0; 1]; C=[-1, 0];

%% Phase portrait plot
figure(1); hold on; grid on;
for x10 = -x100:x10d:x100
    for x20 = -x200:x20d:x200
        x0=[x10;x20];
        sim('relaySimple',Tf);
        plot(xout(:,1),xout(:,2))
        plot(x10,x20,'k*')
    end
end
toc
    