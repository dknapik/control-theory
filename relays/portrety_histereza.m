%% system standard
 A5=[0 1; 0 -.1]; B=[0; 1]; C=[-1 0];
% A5=[0 -pi/2; pi/2 0]; B=[0; 0]; C=[-1 0]; D=[0; 0];
%% oscylacje ver1
% A1=[0 1; -1 0]; B=[0; 0]; C=[-1 -1];
% A2=[1i 0; 0 -1i]; B=[0; 0]; C=[-1 -1];
% A3=[-1i 0; 0 1i]; B=[0; 0]; C=[-1 -1];
% A4=[-.1 -1; 1 -.1]; B=[0; 0]; C=[-1 -1];

A=A5;
%% cykl graniczny
umax=1;
ubias=0;
eps=1;
K=1;

%% rezim slizgowy
% umax=1;
% ubias=0;
% eps=0;
% K=1;

%% plot
figure(1); hold on; grid on;
for x10 = -5:5:5
    for x20 = -2:2:2
        x0=[x10;x20];
        sim('portrety_ss',100);
        plot(xout(:,1),xout(:,2))
        plot(x10,x20,'k*')
    end
end