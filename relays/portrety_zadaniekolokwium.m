%% system standard

% A=[0 -pi/2; pi/2 0]; B=[0; 0]; C=[-1 0]; D=[0; 0];
%A=[0 -2; 1 0]; B=[0; 0]; C=[-1 0]; D=[0; 0];
A=[0 1; 0 0]; B=[0; -1]; C=eye(2); D=[0; 0];
%% cykl graniczny
umax=0;
ubias=0;
eps=0;
K=0;


%% plot
figure(1); hold on; grid on;
for x10 = -1:0.2:1
    for x20 = -1:0.2:1
        if max(abs([x10,x20]))==1
            x0=[x10;x20];
            sim('portrety_ss',10);
            plot(xout(:,1),xout(:,2))
            plot(x10,x20,'k*')
        end
    end
end