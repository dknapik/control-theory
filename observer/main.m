%% init 
A=[0 1; 0 0]; B=[0; 1]; C=[1 0];
x0 = [1; 1]; x0_obser = [0; 0]; x0_numCpy = [.5; .5];
%% calculate observer gain
L=place(A',C',[-2 -2.0000000001])';
L1=acker(A',C',[-2 -2.000000000])';
%%
sim('luenbergerObserver');
plot(T,X.signals(3).values(:,1),T,X.signals(1).values(:,1),T,X.signals(2).values(:,1))
legend('system','luenbergerObserv','numercialCpy')
xlabel('t'),ylabel('x1') 