%% init
Tk=1;
A=[0 1; 0 0]; B=[0; 1]; C=eye(2); D=[0; 0];
tAdj=Tk:-0.01:0;
tSys=0:0.01:1;
opts = odeset('RelTol',1e-10,'AbsTol',1e-10);
%opts = odeset('MaxStep',1e-3)
%% plot
x0=[0;0];
adjointOde = @(t,y) -A'*y; 
tic
for p=0.075:0.1:2*pi
    y0 = [sin(p); cos(p)]; 
    [tAdj,y] = ode45(adjointOde, tAdj, y0);

    figure(4); hold on; grid on;
    u=sign(y)*B;
    [t,y] = ode45(@(t,y) linearSys(t,y,A,B,u,tSys),tSys,[0,0],opts);
    plot(y(:,1),y(:,2));    %plot trajectory
    plot(y(end,1),y (end,2),'d');   %plot boundary point
end
plot(y(1,1),y (1,2),'x') %plot starting point
toc


