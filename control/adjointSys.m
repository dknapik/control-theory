function dydt = adjointSys(~,y,A)
%Adjoint equations for linear system 
% 2020 DK
dydt = A*y;