%% init
Tk=1;
A=[0 1; 0 0]; B=[0; 1]; C=eye(2); D=[0; 0];


%% zbior stanow osiagalnych
t=Tk:-0.01:0;
x0=[0;0];
ode = @(t,y) adjointSys(t,y,-A');
tic
for p=0.075:0.1:2*pi
    y0 = [sin(p); cos(p)];
    
    [t,y] = ode45(ode, t, y0);
    
    if ~isempty(find(diff(sign(y(:,2))), 1))
        T=t(find(diff(sign(y(:,2))))+1);
        U1=sign(y(1,2));
        U2=sign(y(end,2));
    else
        U1=sign(y(1,2));
        U2=U1;
        T=length(y(:,2)); 
    end

    figure(4); hold on; grid on;
    
    sim('osiagalne_lin');
    plot(xout(:,1),xout (:,2)),
    plot(xout(1,1),xout (1,2),'x'), plot(xout(end,1),xout (end,2),'o');
end
toc


