function dydt = linearSys(t,y,A,B,u,ut,u0,swPt)
%linearSys 
% 2020 DK

%% to nie dziala a szkoda
%persistent pozostaje nie tylko pomiedzy ode45 ale rowniez w petli for
% wywolanie inaczej przy testach: function dydt = linearSys(t,y,A,B,u,ut,u0,swPt)

% persistent u i;
% 
% if(isempty(u))
%     u=u0;
%     i=1;    %jesli punktow przelaczen byloby wiecej to tutaj len
% end

% if (i && t>swPt(i))  
%     u=-u;
%     i=i-1;
% end

u=interp1(ut,u,t,'next');
dydt = A*y+B*u;